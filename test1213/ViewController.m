//
//  ViewController.m
//  test1213
//
//  Created by Nazar Starantsov on 23/09/2019.
//  Copyright © 2019 Nazar Starantsov. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@property NSMutableArray *baseArray;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.baseArray = [[NSMutableArray alloc] initWithArray:@[@(3), @(6), @(32), @(24), @(81)]];
    
    // Задание 1.1
    NSLog(@"До сортировки");
    [self printArray: self.baseArray];
    
    [self sortBaseArrayWithMode: YES];
    
    NSLog(@"После сортировки");
    [self printArray: self.baseArray];
    
    // Задание 1.2
    NSLog(@"Массив элементов > 20");
    [self printArray: [self getArrayWithMoreThat20]];
    
    // Задание 1.3
    NSLog(@"Кратные 3м");
    NSMutableArray *dividedBy3Array = [self dividedBy3];
    [self printArray: dividedBy3Array];
    
    // Задание 1.4
    NSLog(@" Добавляем в исходный");
    [self appendArrayToBase: dividedBy3Array];
    [self printArray: self.baseArray];
    
    // Задание 1.5
    NSLog(@" Сортируем смерженный массив по убыванию");
    [self sortBaseArrayWithMode: NO];
    [self printArray: self.baseArray];
    
    // Задание 2
    NSLog(@"Работаем со строками");
    [self arrayRootExtraction];
}

- (void)printArray: (NSMutableArray*)array {
    for (int i = 0; i < array.count; i++)
    {
        NSLog(@"Элемент %d: %@", i, array[i]);
    }
    NSLog(@"\n");
}

- (void)sortBaseArrayWithMode: (BOOL) ascending {
    NSMutableArray *arr = self.baseArray;
    
    for (int i = 0; i < arr.count; i++)
    {
        for(int j = i; j < arr.count; j++)
        {
            if(ascending)
            {
                if([arr[i] integerValue] > [arr[j] integerValue])
                {
                    [arr exchangeObjectAtIndex:i withObjectAtIndex:j];
                }
            }
            else
            {
                if([arr[i] integerValue] < [arr[j] integerValue])
                {
                    [arr exchangeObjectAtIndex:i withObjectAtIndex:j];
                }
            }
            
        }
    }
}

- (NSMutableArray*)getArrayWithMoreThat20 {
    NSMutableArray *arr = [[NSMutableArray alloc] initWithArray: self.baseArray];

    for (int i = 0; i < arr.count; i++)
    {
        if([arr[i] integerValue] < 20)
        {
            [arr removeObjectAtIndex:i];
        }
    }
    return arr;
}

- (NSMutableArray*)dividedBy3 {
    NSMutableArray *arr = [[NSMutableArray alloc] initWithArray: self.baseArray];

    for (int i = 0; i < arr.count; i++)
    {
        if([arr[i] integerValue] % 3)
        {
            [arr removeObjectAtIndex:i];
        }
    }
    return arr;
}

- (void)appendArrayToBase: (NSMutableArray*)arrayOne {
    for(NSObject *object in arrayOne)
    {
        [self.baseArray addObject:object];
    }
}

-(void)arrayRootExtraction
{
    NSArray *initialArr = @[@"cataclism", @"catepillar", @"cat", @"teapot", @"truncate"];
    NSMutableArray *onlyRootWordsArr = [[NSMutableArray alloc] init];

    for(NSString *string in initialArr)
    {
        if ([string  hasPrefix:@"cat"])
        {
            [onlyRootWordsArr addObject:string];
        }
    }
    
    NSLog(@"Базовый массив");
    [self printArray:initialArr];
    
    NSLog(@"Массив содержащий слова с корнем 'cat'");
    [self printArray:onlyRootWordsArr];


    NSMutableDictionary *finalDict = [[NSMutableDictionary alloc] init];
    for( int i = 0; i < [onlyRootWordsArr count]; i++)
    {
        NSString *item = onlyRootWordsArr[i];
        [finalDict setObject: [NSNumber numberWithLong:item.length]
                                 forKey:item];
    }
    
    NSLog(@"");
    
}

@end
